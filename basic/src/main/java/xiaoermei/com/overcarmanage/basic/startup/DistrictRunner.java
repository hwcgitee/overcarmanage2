package xiaoermei.com.overcarmanage.basic.startup;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import xiaoermei.com.overcarmanage.basic.service.IDistrictService;

@Component
public class DistrictRunner implements ApplicationRunner {

    @Autowired
    private IDistrictService districtService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        districtService.rebuildCache();
    }

}
