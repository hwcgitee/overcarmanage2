package xiaoermei.com.overcarmanage.basic.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("xiaoermei.com.overcarmanage.basic.dao.persist.mapper")
public class MyBatisConfiguration {
}
