package xiaoermei.com.overcarmanage.basic.dao.cache;



import xiaoermei.com.common.pojo.DistrictCacheConsts;
import xiaoermei.com.common.pojo.po.DistrictSimplePO;

import java.util.List;

public interface IDistrictCacheRepository extends DistrictCacheConsts {

    // 向缓存中写入省市区的列表
    void saveListByParent(Long parentId, List<DistrictSimplePO> districtList);

    // 删除缓存中所有省市区的数据
    void deleteAll();

    // 从缓存中读取省市区的列表
    List<DistrictSimplePO> listByParent(Long parentId);

    void save(DistrictSimplePO districtSimplePO);
}
