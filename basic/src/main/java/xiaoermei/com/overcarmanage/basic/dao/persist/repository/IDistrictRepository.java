package xiaoermei.com.overcarmanage.basic.dao.persist.repository;



import xiaoermei.com.common.pojo.po.DistrictSimplePO;

import java.util.List;

public interface IDistrictRepository {

    List<DistrictSimplePO> listByParentId(Long parentId);
}
