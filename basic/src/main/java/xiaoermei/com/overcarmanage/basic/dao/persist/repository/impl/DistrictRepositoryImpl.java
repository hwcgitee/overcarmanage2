package xiaoermei.com.overcarmanage.basic.dao.persist.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import xiaoermei.com.common.pojo.po.DistrictSimplePO;
import xiaoermei.com.overcarmanage.basic.dao.persist.mapper.DistrictMapper;
import xiaoermei.com.overcarmanage.basic.dao.persist.repository.IDistrictRepository;

import java.util.List;

@Repository
public class DistrictRepositoryImpl implements IDistrictRepository {
    @Autowired
    DistrictMapper districtMapper;

    @Override
    public List<DistrictSimplePO> listByParentId(Long parentId) {
        return districtMapper.listByParentId(parentId);
    }
}
