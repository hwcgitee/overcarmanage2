package com.xiaoermei.deptmanage.dao.persist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.xiaoermei.deptmanage.pojo.entity.BasepasscarEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface BasepasscarMapper extends BaseMapper<BasepasscarEntity> {
}
