package com.xiaoermei.deptmanage.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.xiaoermei.judegecenter.dao.persist.mapper")
public class MyBatisConfiguration {
}
