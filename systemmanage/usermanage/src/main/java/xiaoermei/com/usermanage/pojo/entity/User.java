package xiaoermei.com.usermanage.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户的实体类
 *
 * @author java@tedu.cn
 * @version 2.0
 */
@Data
@TableName("T_SYSUSER")
public class User implements Serializable {

    /**
     * 用户id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户姓名
     */
    private String name;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码（密文）
     */
    private String password;

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 手机号码
     */
    private String phone;


    /**
     * 是否启用，1=启用，0=未启用
     */
    private Integer enable;



    /**
     * 最后登录IP地址（冗余）
     */
    private String lastLoginIp;

    /**
     * 累计登录次数（冗余）
     */
    private Integer loginCount;

    /**
     * 最后登录时间（冗余）
     */
    private LocalDateTime gmtLastLogin;

    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime gmtCreate;

    /**
     * 数据最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime gmtModified;

}
