package xiaoermei.com.usermanage.pojo.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserAddNewParameter implements Serializable {


    /**
     * 用户名
     */
    private String name;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码（密文）
     */
    private String password;

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 手机号码
     */
    private String phone;


}
