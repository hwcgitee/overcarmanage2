package xiaoermei.com.usermanage.service;

import xiaoermei.com.usermanage.pojo.param.UserAddNewParameter;

public interface IUserService {
    void addNewUser(UserAddNewParameter userAddNewParameter);
}
