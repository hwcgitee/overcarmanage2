package xiaoermei.com.usermanage.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import xiaoermei.com.common.jsonresult.JsonResult;
import xiaoermei.com.usermanage.pojo.param.UserAddNewParameter;
import xiaoermei.com.usermanage.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;



@RestController
@RequestMapping("/usermanage")
@Api(tags = "用户管理")
public class UserController {
    @Autowired
    private IUserService userService;

    @ApiOperation("新增一个用户")
    @ApiOperationSupport(order = 100)
    @PostMapping("/addnewuser")
    public JsonResult addNewUser(@Valid UserAddNewParameter userAddNewParameter, @ApiIgnore HttpServletRequest request) {
        userService.addNewUser(userAddNewParameter);
        return JsonResult.ok();

    }
}
