package xiaoermei.com.usermanage.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("xiaoermei.com.usermanage.dao.persist.mapper")
public class MyBatisConfiguration {
}
