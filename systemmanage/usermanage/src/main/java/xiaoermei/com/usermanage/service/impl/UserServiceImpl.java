package xiaoermei.com.usermanage.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import xiaoermei.com.usermanage.dao.persist.repository.IUserRepository;
import xiaoermei.com.usermanage.pojo.entity.User;
import xiaoermei.com.usermanage.pojo.param.UserAddNewParameter;
import xiaoermei.com.usermanage.service.IUserService;

public class UserServiceImpl implements IUserService {
    @Autowired
    private IUserRepository userRepository;

    @Override
    public void addNewUser(UserAddNewParameter userAddNewParameter) {
        User newUser=new User();
        BeanUtils.copyProperties(userAddNewParameter,newUser);
        Integer addResult=userRepository.addNewUser(newUser);


        return ;
    }
}
