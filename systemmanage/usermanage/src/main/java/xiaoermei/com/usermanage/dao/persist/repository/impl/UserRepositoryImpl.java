package xiaoermei.com.usermanage.dao.persist.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import xiaoermei.com.usermanage.dao.persist.mapper.UserMapper;
import xiaoermei.com.usermanage.dao.persist.repository.IUserRepository;
import xiaoermei.com.usermanage.pojo.entity.User;


import java.time.LocalDateTime;

@Repository
public class UserRepositoryImpl implements IUserRepository {
    @Value("${tmall.jwt.duration-in-minute}")
    private long durationInMinute;

    @Autowired
    private UserMapper userMapper;



    @Override
    public int addNewUser(User newUser) {
       int addResult=userMapper.insert(newUser);
        return addResult;
    }


}
