package com.xiaoerimei.sitemanagement.dao.persist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.xiaoerimei.sitemanagement.pojo.entity.BasepasscarEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface BasepasscarMapper extends BaseMapper<BasepasscarEntity> {
}
