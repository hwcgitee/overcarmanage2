package com.xiaoermei.rolemanage.dao.persist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.xiaoermei.rolemanage.pojo.entity.BasepasscarEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface BasepasscarMapper extends BaseMapper<BasepasscarEntity> {
}
