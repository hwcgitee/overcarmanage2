package xiaoermei.com.passport.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("xiaoermei.com.passport.dao.persist.mapper")
public class MyBatisConfiguration {
}
