package xiaoermei.com.passport.service;


import xiaoermei.com.common.authentication.CurrentPrincipal;
import xiaoermei.com.passport.pojo.param.UserLoginInfoParam;
import xiaoermei.com.passport.pojo.vo.UserLoginResultVO;

public interface IUserService {

    UserLoginResultVO login(UserLoginInfoParam userLoginInfoParam, String remoteAddr, String userAgent);

    void logout(CurrentPrincipal currentPrincipal);


}
