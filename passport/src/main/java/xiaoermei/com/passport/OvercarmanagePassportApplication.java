package xiaoermei.com.passport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvercarmanagePassportApplication {
    public static void main(String[] args) {
        SpringApplication.run(OvercarmanagePassportApplication.class,args);
    }
}
