package xiaoermei.com.passport.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 用于登录成功之后给客户端响应的内容
 */
@Data
public class UserLoginResultVO implements Serializable {

    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;



    /**
     * Token（JWT）
     */
    private String token;

    /**
     * 权限列表
     */
    private List<String> authorities;

}
