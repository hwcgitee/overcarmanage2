package xiaoermei.com.passport.dao.persist.repository.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import xiaoermei.com.passport.dao.persist.mapper.LoginLogMapper;
import xiaoermei.com.passport.dao.persist.repository.ILoginLogRepository;
import xiaoermei.com.passport.pojo.entity.LoginLog;

@Repository
public class LoginLogRepositoryImpl implements ILoginLogRepository {
    @Autowired
    LoginLogMapper loginLogMapper;

    @Override
    public int insert(LoginLog loginLog) {
        return loginLogMapper.insert(loginLog);
    }
}
