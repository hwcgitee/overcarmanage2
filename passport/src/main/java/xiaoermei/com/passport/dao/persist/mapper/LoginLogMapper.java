package xiaoermei.com.passport.dao.persist.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import xiaoermei.com.passport.pojo.entity.LoginLog;

@Repository
public interface LoginLogMapper extends BaseMapper<LoginLog> {
}
