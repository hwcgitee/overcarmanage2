package xiaoermei.com.passport.dao.persist.repository;


import xiaoermei.com.passport.pojo.entity.LoginLog;

public interface ILoginLogRepository {
    int insert(LoginLog loginLog);
}
