package xiaoermei.com.passport.dao.persist.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import xiaoermei.com.passport.pojo.entity.User;
import xiaoermei.com.passport.pojo.vo.UserLoginInfoVO;

@Repository
public interface UserMapper extends BaseMapper<User> {



    UserLoginInfoVO getLoginInfoByUsername(String username);

}
