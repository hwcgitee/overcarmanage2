package xiaoermei.com.passport.dao.persist.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import xiaoermei.com.passport.dao.persist.mapper.UserMapper;
import xiaoermei.com.passport.dao.persist.repository.IUserRepository;
import xiaoermei.com.passport.pojo.entity.User;
import xiaoermei.com.passport.pojo.vo.UserLoginInfoVO;

import java.time.LocalDateTime;

@Repository
public class UserRepositoryImpl implements IUserRepository {
    @Value("${tmall.jwt.duration-in-minute}")
    private long durationInMinute;

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserLoginInfoVO getLoginInfoByUsername(String username) {
        return userMapper.getLoginInfoByUsername(username);
    }

    @Override
    public int updateLastLogin(Long id, Integer totalLoginCount,String remoteAddr, LocalDateTime now) {
        User user=new User();
        user.setId(id);
        user.setLoginCount(totalLoginCount);
        user.setLastLoginIp(remoteAddr);
        user.setGmtLastLogin(now);
        return userMapper.updateById(user);
    }




}
