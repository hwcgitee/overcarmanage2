package xiaoermei.com.dataup.api;

public interface UploadIApi {
    /**
     *
     * @param localPath 文件的唯一地址，前提是网络要通
     */
    void upload(String localPath);
}
