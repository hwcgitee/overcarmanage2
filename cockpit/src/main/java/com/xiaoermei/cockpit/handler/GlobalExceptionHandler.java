package com.xiaoermei.cockpit.handler;



import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import xiaoermei.com.common.jsonresult.JsonResult;
import xiaoermei.com.common.jsonresult.enumerator.ServiceCode;
import xiaoermei.com.common.jsonresult.ex.ServiceException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public JsonResult handleServiceException(ServiceException e) {
        return JsonResult.fail(e);
    }

    @ExceptionHandler
    public JsonResult handleBindException(BindException e) {
        // List<FieldError> fieldErrors = e.getFieldErrors();
        // for (FieldError fieldError : fieldErrors) {
        //     fieldError.getDefaultMessage(); // 拼接
        // }
        String message = e.getFieldError().getDefaultMessage();
        return JsonResult.fail(ServiceCode.ERROR_BAD_REQUEST, message);
    }

//    没有对应的权限时Spring Security框架会抛出AccessDeniedException异常
    @ExceptionHandler
    public JsonResult handleAccessDeniedException(AccessDeniedException e) {
        String message = "操作失败，您当前登录的账号无此操作权限！";
        return JsonResult.fail(ServiceCode.ERROR_FORBIDDEN, message);
    }

    @ExceptionHandler
    public JsonResult handleThrowable(Throwable e) {
        log.error("程序运行过程中出现了未处理的异常：", e);
        String message = "服务器忙，请稍后再试！【开发阶段：同学们，当你看到这句话时，你应该通过服务器端的控制台了解本次出现异常的原因，并添加针对处理此异常的方法】";
        return JsonResult.fail(ServiceCode.ERROR_UNKNOWN, message);
    }

}
