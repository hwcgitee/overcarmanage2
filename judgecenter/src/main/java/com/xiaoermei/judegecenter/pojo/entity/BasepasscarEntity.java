package com.xiaoermei.judegecenter.pojo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * t_bas_passcar_data 表的实体类
 */
@Data
public class BasepasscarEntity implements Serializable {
    private Long passcarId;
    private String recordCode;
    private String carNo;
    private Integer carNoColor;
    private BigDecimal totalWeight;
    private BigDecimal limitWeight;
    private BigDecimal overrun;
    private Integer axis;
    private String axisType;
    private String laneCode;
    private Integer siteType;
    private String siteName;
    private Integer carType;
    private Integer carColor;
    private Integer direction;
    private BigDecimal overrunRate;
    private BigDecimal speed;
    private Integer blackList;
    private Integer dxCount;
    private Integer xhCount;
    private String linkMan;
    private String phone;
    private String carHolderAddr;
    private Integer back;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date insertTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ValidTime;
    private String photo;
    private String photo1;
    private String photo2;
    private String photo3;
    private String photo4;
    private String photo5;
    private String video;
    private String areaProvince;
    private String areaCity;
    private String areaCounty;
    private String areaDept;
    private Integer isOverrun;
    private Integer isTruck;
    private String content;
    private String auditor;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date auditTime;
    private Integer status;
}
