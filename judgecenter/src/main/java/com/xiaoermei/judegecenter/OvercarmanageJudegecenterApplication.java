package com.xiaoermei.judegecenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvercarmanageJudegecenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(OvercarmanageJudegecenterApplication.class, args);
    }

}
