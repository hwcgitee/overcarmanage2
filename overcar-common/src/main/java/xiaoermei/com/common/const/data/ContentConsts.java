package xiaoermei.com.common.pojo;

/**
 * 内容管理相关常量
 *
 * @author java@tedu.cn
 * @version 2.0
 */
public interface ContentConsts extends xiaoermei.com.common.pojo.CommonConsts {

    /**
     * 资源类型：文章
     */
    int RESOURCE_TYPE_ARTICLE = 0;
    /**
     * 资源类型：评论
     */
    int RESOURCE_TYPE_COMMENT = 1;

    /**
     * 操作类型：踩
     */
    int OP_TYPE_DOWN = 0;
    /**
     * 操作类型：顶
     */
    int OP_TYPE_UP = 1;

}
