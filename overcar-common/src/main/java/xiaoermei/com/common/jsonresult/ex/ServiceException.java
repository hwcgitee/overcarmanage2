package xiaoermei.com.common.jsonresult.ex;



import lombok.Getter;

/**
 * 业务异常
 *
 * @author java@tedu.cn
 * @version 2.0
 */
public class ServiceException extends RuntimeException {

    @Getter
    private xiaoermei.com.common.jsonresult.enumerator.ServiceCode serviceCode;

    /**
     * 创建业务异常对象
     *
     * @param serviceCode 业务状态码
     * @param message     描述文本
     */
    public ServiceException(xiaoermei.com.common.jsonresult.enumerator.ServiceCode serviceCode, String message) {
        super(message);
        this.serviceCode = serviceCode;
    }

}
