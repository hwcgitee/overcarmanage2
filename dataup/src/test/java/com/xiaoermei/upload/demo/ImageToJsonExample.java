package com.xiaoermei.upload.demo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.awt.*;

/**
 * demo-将图片转换为JSON,然后把JSON转成图片存储；包含知识点：Base64和Gson的知识
 */
public class ImageToJsonExample {

    public static void main(String[] args) {

        String imagePath = "/Users/hewenchao/Pictures/hanhan.jpeg";

        // 将图片转换为JSON
        String json = convertImageToJson(imagePath);
        System.out.println("图片转换为JSON：" + json);

        // 将JSON还原为图片
        String restoredImagePath = "/Users/hewenchao/Pictures/restored_image.png";
        restoreJsonToImage(json, restoredImagePath);
        System.out.println("JSON还原为图片成功");



    }

    // 将图片转换为JSON
    private static String convertImageToJson(String imagePath) {
        try {
            // 读取图片文件
            BufferedImage image = ImageIO.read(new File(imagePath));

            // 创建字节输出流，用于写入Base64编码后的图片数据
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);

            // 对图片数据进行Base64编码
            byte[] imageData = baos.toByteArray();
            String base64Image = Base64.getEncoder().encodeToString(imageData);

            // 构建包含图片数据的JSON对象
            ImageJsonData jsonData = new ImageJsonData();
            jsonData.setImageData(base64Image);

            // 将JSON对象转换为JSON字符串
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 将JSON还原为图片
    private static void restoreJsonToImage(String json, String restoredImagePath) {
        try {
            // 解析JSON字符串为JSON对象
            Gson gson = new Gson();
            ImageJsonData jsonData = gson.fromJson(json, ImageJsonData.class);

            // 对Base64编码的图片数据进行解码
            byte[] imageData = Base64.getDecoder().decode(jsonData.getImageData());

            // 创建字节输入流，用于读取图片数据
            ByteArrayInputStream bais = new ByteArrayInputStream(imageData);

            // 读取图片数据并生成图片文件
            BufferedImage restoredImage = ImageIO.read(bais);
            ImageIO.write(restoredImage, "png", new File(restoredImagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    // 定义用于存储图片数据的JSON对象
    private static class ImageJsonData {
        private String imageData;

        public String getImageData() {
            return imageData;
        }

        public void setImageData(String imageData) {
            this.imageData = imageData;
        }
    }


}
