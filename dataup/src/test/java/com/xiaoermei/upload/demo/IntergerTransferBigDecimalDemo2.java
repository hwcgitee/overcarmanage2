package com.xiaoermei.upload.demo;

import java.math.BigDecimal;

/**
 *如何把Interger的类型12345转成BigDecimal的123.45
 */
public class IntergerTransferBigDecimalDemo2 {
    public static void main(String[] args) {

        Integer num = 12345;
        BigDecimal divisor = new BigDecimal("100");

        BigDecimal decimal2 = new BigDecimal(num).divide(divisor);

        System.out.println(decimal2);


       Integer number = 98101;
        //setScale()是设置小数位的位数
        BigDecimal decimal = new BigDecimal(number).setScale(3);//98101.000
        System.out.println(decimal);
        //这是用double的类型
        double dou=  number / 1000.0;//98101.000
        System.out.println(decimal);
        double roundedDecimal = Math.round(dou * 1000.0) / 1000.0; // 保留三位小数，四舍五入

        System.out.println(roundedDecimal);
    }

    /**
     * 问--Math.round()方法有什么用
     * Math.round() 方法是 Java 中 Math 类提供的一个方法，用于执行数值的四舍五入操作。它接受一个浮点数参数，并返回最接近此参数的整数，其中四舍五入到最接近的整数。
     *
     * round() 方法的语法如下：
     *
     * public static long round(double num)
     * 其中 num 是需要进行四舍五入的浮点数。
     *
     * 例如，如果调用 Math.round(3.14)，该方法将返回 3，因为 3.14 最接近的整数是 3。同样地，如果调用 Math.round(3.8)，该方法将返回 4，因为 3.8 最接近的整数是 4。
     *
     * 在实际编程中，Math.round() 方法通常用于对浮点数进行舍入，以保留指定的小数位数。例如，如果要将一个浮点数保留到小数点后两位，则可以使用以下代码：
     *
     * java
     * double number = 3.14159265359;
     * double roundedNumber = Math.round(number * 100.0) / 100.0; // 保留两位小数，四舍五入
     *
     * System.out.println(roundedNumber); // 输出结果为 3.14
     * 在上面的示例中，我们将浮点数 3.14159265359 乘以 100.0，使小数点向右移动两位。然后，我们使用 Math.round() 方法将其舍入到最接近的整数，最后再将其除以 100.0，使小数点回到原来的位置，以实现保留两位小数的效果。
     */

}
