package com.xiaoermei.upload.demo;

import java.io.File;

/**
 * 使用File的getName方法获取一个路径（String)中的文件全名
 */
public class GetFilename {
    public static void main(String[] args) {

        String filePath = "/Users/hewenchao/Downloads/software/testmultifiles/photo/1.jpg";

        // 创建 File 对象
        File file = new File(filePath);

        // 获取文件名
        String fileName = file.getName();

        System.out.println(fileName);  // 输出：1.jpg
    }

}
