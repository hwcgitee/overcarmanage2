package com.xiaoermei.dataup.service.impl;


import com.xiaoermei.dataup.dao.cache.ISiteCacheRepository;
import com.xiaoermei.dataup.dao.persist.repository.IBasepasscarRepository;
import com.xiaoermei.dataup.pojo.dto.BasepasscarUpDTO;
import com.xiaoermei.dataup.pojo.dto.MultipartFileUploadDTO;
import com.xiaoermei.dataup.pojo.entity.BasepasscarEntity;
import com.xiaoermei.dataup.service.IBasepasscarService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import xiaoermei.com.common.jsonresult.enumerator.ServiceCode;
import xiaoermei.com.common.jsonresult.ex.ServiceException;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;


@Slf4j
@Service
public class BasepasscarServiceImpl implements IBasepasscarService {
    @Autowired
    private IBasepasscarRepository basepasscarRepository;
    @Autowired
    private ISiteCacheRepository siteCacheRepository;
    //阿里云照片的域名  todo 和定义在方法中有什么区别，选择哪个更好
    public static final String ALT_IMG_DOMAIN = "https://overcarbucket.oss-cn-hangzhou.aliyuncs.com/"+"MultipartFiles/";


    @Autowired
    private RocketMQTemplate rocketMQTemplate;


    @Override
    public int uploadPassVehicle(BasepasscarUpDTO basepasscarUpDTO) {

        //第一大步,数据库中有要求的也在在service层的判断得到体现
        BasepasscarEntity basepasscarEntity=new BasepasscarEntity();




        //todo 第二、三步需要处理的内容：字段名称不一致的；字段值的类型不一样的需要转换；必要的验证
        //第二大步，处理前端传过来的数据
        //todo 咱不处理的数据-非必传  "vehicleBrand": "车辆品牌"；"车辆类型" //填写车辆类型代码
        //todo checkType 检测数据类型，现在的项目要求这个字段不用处理
        //vehicleNoType  号牌种类  todo 暂不处理
        //checkNo 检测单号,切割字符串，前面一半得到站点编码，后面一半得到检测时间,下面有，这里就不用单独处理了
        basepasscarEntity.setRecordCode(basepasscarUpDTO.getCheckNo());
        //siteCode 站点编号，现在就按插入的数据1,2，,3来处理; todo 标准做法是先去表里查一下对应的站点，然后根据得到的信息进行判断，下面已经实现了，这里是无效代码

        if (basepasscarUpDTO.getSiteCode()=="1"){
            basepasscarEntity.setSiteName("兰溪非现场点位1");
        };
        if (basepasscarUpDTO.getSiteCode()=="2"){
            basepasscarEntity.setSiteName("兰溪非现场点位1");
        };
        if (basepasscarUpDTO.getSiteCode()=="3"){
            basepasscarEntity.setSiteName("兰溪非现场点位1");
        };
        if (basepasscarUpDTO.getSiteCode()=="4"){
            basepasscarEntity.setSiteName("兰溪非现场点位1");
        };

        //siteType “站点类型”，现在的项目要求就是31
        if (basepasscarUpDTO.getSiteType()==31){
            basepasscarEntity.setSiteType(31);
        } else {
            String message = "该站点类型不存在";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        //laneNumber 检测车道编号,现在就1,2两个来（默认是330国道的双向2车道的基础先实现
        basepasscarEntity.setLaneCode(basepasscarUpDTO.getLaneNumber());
        //检测时间 checkTime （demo1）居然没有用上，搞笑
        //vehicleNo  车牌号
        basepasscarEntity.setCarNo(basepasscarUpDTO.getVehicleNo());
        //vehicleColor 车牌颜色
        basepasscarEntity.setCarNoColor(basepasscarEntity.getCarNoColor());
        //travelDirection  行驶方向
        basepasscarEntity.setDirection(basepasscarUpDTO.getTravelDirection());
        // speed   车速,这个自身就矛盾了，改接口
        basepasscarEntity.setSpeed(basepasscarUpDTO.getSpeed());
        //axles  车轴数量
        basepasscarEntity.setAxis(basepasscarUpDTO.getAxles());
        //axlesType  轴组类型
        basepasscarEntity.setAxisType(basepasscarUpDTO.getAxlesType());
        //total  车货总质量，demo2
        BigDecimal divisor = new BigDecimal("1000");
        BigDecimal total = new BigDecimal(basepasscarUpDTO.getTotal()).divide(divisor);
        basepasscarEntity.setTotalWeight(total);

        //limitWeight  最大允许总质量,政策改了之后这个接口按理也要更新，这个值应该是后台来设置的
        BigDecimal limitWeight = new BigDecimal(basepasscarUpDTO.getTotal()).divide(divisor);
        basepasscarEntity.setLimitWeight(limitWeight);

        //overWeight  超限量
        if(basepasscarUpDTO.getOverWeight()>0){
            BigDecimal overWeight = new BigDecimal(basepasscarUpDTO.getOverWeight()).divide(divisor);
            basepasscarEntity.setOverrun(overWeight);
            //overRate  超限超载率
            basepasscarEntity.setOverrunRate(basepasscarUpDTO.getOverRate());
            //第三大步要做的赋值
            basepasscarEntity.setIsOverrun(1);
            //第三大步要做的赋值--status;不超限是9，超限是1
            basepasscarEntity.setIsOverrun(1);
        }else {
            basepasscarEntity.setIsOverrun(0);
            basepasscarEntity.setIsOverrun(0);
        }




        //第三大步：处理后台填充的数据
        //passcarId 默认生成即可
        //siteName 需要查询得到,这个可以放redis中--下一步就是考虑策略的事情
        if(basepasscarUpDTO.getSiteCode()!=null ){
            //从redis中获取站点名称
            String sitename=siteCacheRepository.getSitenameBySitecode(basepasscarUpDTO.getSiteCode());
            basepasscarEntity.setSiteName(sitename);
        }
        //carType todo 暂不处理
        //blackList todo 需要查redis看是不是是否有已告知和未结案的记录，后期布控车的功能也需要用到
        //dxCount,按着数据库的默认值来就行了
        //xhCount,按着数据库的默认值来就行了
        //linkMan todo 优先查本地数据库:其实赋值可以放到初审通过之后（放redis?,其实看数据的压力了），后查询接口得到的（这个某种程度是不存在的）--省平台是判定之后
        //phone todo
        //carHolderAddr todo
        //back,按着数据库的默认值来就行了
        // updateTime 通过mybatis plus的注解完成
        //insertTime;通过mybatis plus的注解完成
        // ValidTime,政策不改就按着数据库的默认值来就行了
       //  areaProvince,应该在service层做设置，其实数据库里也可以设置为默认值
       basepasscarEntity.setAreaProvince("浙江省");
       // areaCit;,应该在service层做设置，其实数据库里也可以设置为默认值
        basepasscarEntity.setAreaCity("金华市");
       //  areaCounty;应该在service层做设置，其实数据库里也可以设置为默认值
        basepasscarEntity.setAreaCounty("兰溪市");
       //  areaDept;按理应该根据数据库查询得到站点所在部门，其实也没有必要查，能不查就不查
        basepasscarEntity.setAreaDept("兰溪市交通运输综合行政执法队");
        // isOverrun;可以放到前面超限的里面去
        // isTruck;标准暂定是3吨,这个判断依据是不充分的
        if (basepasscarUpDTO.getTotal()>30000){
            basepasscarEntity.setIsTruck(1);
        }else {
            basepasscarEntity.setIsTruck(0);
        }
        //content;按着数据库的默认值来就行了
        // auditor;按着数据库的默认值来就行了
        // auditTime;数据库设置默认为null  todo 如果放这些经常变的数据源，那索引变动太大了





//        第四大步
        int num=basepasscarRepository.uploadPassVehicle(basepasscarEntity);
        return num;



    }

    // CredentialsProviderFactory.newEnvironmentVariableCredentialsProvider()--因为try了就不能赋值了所以跑出来，最终有全局异常处理器的
    @Override
    public void uploadImgMultipartFiles(MultipartFileUploadDTO multipartFileUploadDTO) throws com.aliyuncs.exceptions.ClientException {

//第一大步是把非结构化相关字段存入到mariadb中,等于是在做一个更新的操作
//        //todo 这里是否可以优化的，因为用完同一个数据下次就不用了
       BasepasscarEntity basepasscarEntity=new BasepasscarEntity();
////        对multipartFileUploadDTO的处理
//        //checkNo 检测单号,从数据库里面查出一样的数据，然后在这个记录中再把其他信息中放进去，todo 是否存在照片先传上来的情况，那检测单号不存在就创建新的记录，
//        //todo 从优化的角度，这个必须要走redis不然数据库的查询要求太高了,也没有丢失数据的风险
        basepasscarEntity.setRecordCode(multipartFileUploadDTO.getCheckNo());
//
//        //transType，照片传输方式--todo 这个不清楚，反推就是非结构的JSON数据了
//        //siteType 站点类型，设置为31就好了,可以做个判断但是对数据库里面存的数据没有影响
//
//        //vehiclepic1-6，参考从存数据库存的那些内容来设置
//        basepasscarEntity.setPhoto(multipartFileUploadDTO.getVehiclepic1());
//        basepasscarEntity.setPhoto1(multipartFileUploadDTO.getVehiclepic2());
//        basepasscarEntity.setPhoto2(multipartFileUploadDTO.getVehiclepic3());
//        basepasscarEntity.setPhoto3(multipartFileUploadDTO.getVehiclepic4());
//        basepasscarEntity.setPhoto4(multipartFileUploadDTO.getVehiclepic5());
//        basepasscarEntity.setPhoto5(multipartFileUploadDTO.getVehiclepic6());
//        //shortvedio  短视频
//        basepasscarEntity.setVideo(multipartFileUploadDTO.getShortvedio());
        //suffixType  视频后缀类型  1:mp4 格式 ；2：avi 格式 其他的再--某种程度上没有什么用

//      需要后台处理的





        //第二大步是把真实的非结构化数据存储到OSS中--从实现的角度这个变成第一大步了
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("/yyyy/MM/dd/");
        String datepath = simpleDateFormat.format(new Date());

        //todo  标准做法,1、列举存储空间，2、判断存储空间是否存在，3、不存在就创建（要先明确分类的标准）; 4、上传：上传分很多种（看场景解决），按理是他们先传给我们，我们再上传，这里做的就是自己去取数据了
        //todo 关于上传，如果传一半没有传成功可以有2种做法，一个是追加上传是指通过AppendObject方法在已上传的追加类型文件（Appendable Object）末尾直接追加内容。一个是整个文件都重传（反正会覆盖）
        //todo 接收到前端收到的文件之后是直接传还是本地要存一份--看需求，可以是本地先存再传省里的（某种程度不符合要求），我们是直接传，模拟是自己去读本地的数据
        //使用简单上传--（上传文件流的方式,标准方式是用流扫描读取文件夹下文件--获取文件名--得到流水号，是哪一种类型的照片or视频，按照接口文档要求传（同一个流水号的组成一个JSSON传）；
        // 优化需求是实现同时传给多个地方；），
        // 还有一个要求是可以直接写个返回值（先试这种方式），可以是上传回调指定一个回调函数
        //todo 可以写一个定时任务来造数据


        //todo 可以把上传到oss的功能做异步，对数据对接的效果会更好（线程的消耗是大的，超限数据其实没有那么多），对用户是无感的/风险在于数据的不一致性，是否会大大减轻mariadb的效率？--需要测试了
        //todo 下面的代码从现在看还可以做一次封装,AOP（但是某种程度上这个也等于是固定死了，不灵活）
        if (multipartFileUploadDTO.getVehiclepic1()!=null){
            //todo 本地路径可以放到配置文件中
           String localPath= "/Users/hewenchao/Downloads/software/testmultifiles/photo/"+multipartFileUploadDTO.getVehiclepic1();
            log.debug("开始上传车牌照了，参数：{}", localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
           //UploadMutltifilesToOSS.upload(localPath);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getVehiclepic1();
            basepasscarEntity.setPhoto(OSSPath);
        }
        if (multipartFileUploadDTO.getVehiclepic2()!=null){
            //todo 本地路径可以放到配置文件中
            String localPath= "D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\photo\\"+multipartFileUploadDTO.getVehiclepic2();
            log.debug("开始上传车头照了，参数：{}", localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
            log.debug("车头照上传完成，返回的参数：{}", sendResult);
            //UploadMutltifilesToOSS.upload(localPath);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getVehiclepic2();
            basepasscarEntity.setPhoto1(OSSPath);
        }
        if (multipartFileUploadDTO.getVehiclepic3()!=null){
            //todo 本地路径可以放到配置文件中
            String localPath="D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\photo\\"+multipartFileUploadDTO.getVehiclepic3();
            log.debug("开始上传车侧照了，参数：{}", localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
            //UploadMutltifilesToOSS.upload(localPath);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getVehiclepic3();
            basepasscarEntity.setPhoto2(OSSPath);
        }
        if (multipartFileUploadDTO.getVehiclepic4()!=null){
            //todo 本地路径可以放到配置文件中
            String localPath= "D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\photo\\"+multipartFileUploadDTO.getVehiclepic4();
            log.debug("开始上传车尾照了，参数：{}", localPath);
           // UploadMutltifilesToOSS.upload(localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getVehiclepic4();
            basepasscarEntity.setPhoto3(OSSPath);
        }
        if (multipartFileUploadDTO.getVehiclepic5()!=null){
            //todo 本地路径可以放到配置文件中
            String localPath="D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\photo\\"+multipartFileUploadDTO.getVehiclepic5();
            log.debug("开始上传前侧照了，参数：{}", localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
          //  UploadMutltifilesToOSS.upload(localPath);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getVehiclepic5();
            basepasscarEntity.setPhoto4(OSSPath);
        }
        if (multipartFileUploadDTO.getVehiclepic6()!=null){
            //todo 本地路径可以放到配置文件中
            String localPath="D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\photo\\"+multipartFileUploadDTO.getVehiclepic6();
            log.debug("开始上传后侧照了，参数：{}", localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
           // UploadMutltifilesToOSS.upload(localPath);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getVehiclepic6();
            basepasscarEntity.setPhoto5(OSSPath);
        }

        if (multipartFileUploadDTO.getShortvedio()!=null){
            //todo 本地路径可以放到配置文件中
            String localPath="D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\video\\"+multipartFileUploadDTO.getShortvedio();
            log.debug("开始上传短视频了，参数：{}", localPath);
            Message<String> message=
                    MessageBuilder.withPayload(localPath).build();
            SendResult sendResult = rocketMQTemplate.syncSend("overcarmanage-dataup-topic:uploadImgMultipartFiles", message);
           // UploadMutltifilesToOSS.upload(localPath);
            String OSSPath=ALT_IMG_DOMAIN+multipartFileUploadDTO.getShortvedio();
            basepasscarEntity.setVideo(OSSPath);
        }


        basepasscarRepository.updatemultipartFiles(basepasscarEntity);

    }



}
