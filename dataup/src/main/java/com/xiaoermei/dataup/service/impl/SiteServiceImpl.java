package com.xiaoermei.dataup.service.impl;

import com.xiaoermei.dataup.dao.cache.ISiteCacheRepository;
import com.xiaoermei.dataup.dao.persist.repository.ISiteRepository;
import com.xiaoermei.dataup.pojo.po.SitenamePO;
import com.xiaoermei.dataup.service.ISiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SiteServiceImpl implements ISiteService {
    @Autowired
    private ISiteCacheRepository siteCacheRepository;
    @Autowired
    private ISiteRepository siteRepository;

    @Override
    public void rebuildSitenameSearch() {
          // todo 要先删除原来的数据
        siteCacheRepository.deleteAll();


        //从数据库查出List<SitenamePO>
        List<SitenamePO> sitenamePOS=siteRepository.ListAllSiteCache();


          siteCacheRepository.rebuildSitenameSearch(sitenamePOS);
          return;
    }
}
