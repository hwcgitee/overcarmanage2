package com.xiaoermei.dataup.service;

import com.aliyuncs.exceptions.ClientException;
import com.xiaoermei.dataup.pojo.dto.BasepasscarUpDTO;
import com.xiaoermei.dataup.pojo.dto.MultipartFileUploadDTO;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


@Transactional
public interface IBasepasscarService {
    int uploadPassVehicle(BasepasscarUpDTO basepasscarUpDTO);

    void uploadImgMultipartFiles(MultipartFileUploadDTO multipartFileUploadDTO) throws ClientException;


}
