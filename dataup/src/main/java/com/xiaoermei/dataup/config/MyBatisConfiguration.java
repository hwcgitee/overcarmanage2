package com.xiaoermei.dataup.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.xiaoermei.dataup.dao.persist.mapper")
public class MyBatisConfiguration {
}
