package com.xiaoermei.dataup.consumer;

import com.xiaoermei.dataup.util.UploadMutltifilesToOSS;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;


@Component
@RocketMQMessageListener(topic="overcarmanage-dataup-topic",consumerGroup = "overcarmanager-imgdataup-consumer")
@Slf4j
/**
 * todo 如何消费指定tag的消息，这里不配tag属性是能完成消费带了tag的消息的
 */
public class  uploadImgMultipartFilesConsumer implements RocketMQListener<String> {
    @Override
    public void onMessage(String localPath) {
        log.info("数据上传收到了消费对象:{}",localPath);
        UploadMutltifilesToOSS.upload(localPath);

    }
}
