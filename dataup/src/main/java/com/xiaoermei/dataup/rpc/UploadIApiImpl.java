package com.xiaoermei.dataup.rpc;


import com.xiaoermei.dataup.util.UploadMutltifilesToOSS;
import org.springframework.stereotype.Component;
import xiaoermei.com.dataup.api.UploadIApi;


@Component
public class UploadIApiImpl implements UploadIApi {
    @Override
    public void upload(String localPath) {
        UploadMutltifilesToOSS.upload(localPath);
    }
}
