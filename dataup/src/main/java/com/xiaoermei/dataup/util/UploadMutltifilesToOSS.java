package com.xiaoermei.dataup.util;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;

import java.io.*;

@Slf4j
public class UploadMutltifilesToOSS {
    /**
     * 通过文件路径上传文件到OSS，  todo 实际上最终用的InputStream，所以传参可以设置为InputStream
     * @param localPath  文件路径
     */
    public static void upload(String localPath){
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "https://oss-cn-hangzhou.aliyuncs.com/";
        // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。

        //EnvironmentVariableCredentialsProvider credentialsProvider  = CredentialsProviderFactory.newEnvironmentVariableCredentialsProvider();

        // 填写Bucket名称，例如examplebucket。
        String bucketName = "overcarbucket";

        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
        String filePath= localPath;
        //切割localPath得到文件名
        String filename=filePath.substring(filePath.lastIndexOf("/") + 1);
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。  todo 根据传参的不一样把不同数据放到oss不同的目录下
        String objectName = "MultipartFiles/"+filename;

        // 创建OSSClient实例。
        //OSS ossClient = new OSSClientBuilder().build(endpoint, credentialsProvider);
        //key和密码
        String accessKeyId = "LTAI5tEDo5URDa49eaXRRYSM";
        String accessKeySecret = "jwUnYyusI6G19AzDs2DLHipUyHNcbK";
        //OSS客户端对象创建
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {
            InputStream inputStream = new FileInputStream(filePath);

            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, inputStream);
            //传参是文件路径的，传文件的时候也可以通过传File的参数进行文件上传到OSS
            //PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, new File(filePath));
            // 创建PutObject请求。
            PutObjectResult result = ossClient.putObject(putObjectRequest);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
       return;
    }

    /**
     *
     * 上传字节数组到OSS的指定文件中
     * @param content  字节数组，如果上传的字符串需要先转成字节数组然后再传参
     */
    public static void upload(byte[] content){
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "https://oss-cn-hangzhou.aliyuncs.com/";
        // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。

        //EnvironmentVariableCredentialsProvider credentialsProvider  = CredentialsProviderFactory.newEnvironmentVariableCredentialsProvider();

        // 填写Bucket名称，例如examplebucket。
        String bucketName = "overcarbucket";
        //todo 字节数组传到OSS的的哪个文件根据业务需求进行调整
        File filename=new File("uuid.png");
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。  todo 根据传参的不一样把不同数据放到oss不同的目录下
        String objectName = "String/"+filename;

        // 创建OSSClient实例。
        //OSS ossClient = new OSSClientBuilder().build(endpoint, credentialsProvider);
        //key和密码
        String accessKeyId = "LTAI5tEDo5URDa49eaXRRYSM";
        String accessKeySecret = "jwUnYyusI6G19AzDs2DLHipUyHNcbK";
        //OSS客户端对象创建
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {


            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, new ByteArrayInputStream(content));
            // 创建PutObject请求。
            PutObjectResult result = ossClient.putObject(putObjectRequest);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return;
    }
}
