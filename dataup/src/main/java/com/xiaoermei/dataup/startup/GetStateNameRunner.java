package com.xiaoermei.dataup.startup;


import com.xiaoermei.dataup.service.ISiteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 处理商品搜索数据的预加载
 *
 * @author java@tedu.cn
 * @version 2.0
 */
@Slf4j
@Component
public class GetStateNameRunner implements ApplicationRunner {

    @Autowired
    private ISiteService siteService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.debug("开始执行【重建站点编号和站点名称】的数据预热");
        siteService.rebuildSitenameSearch();
    }

}
