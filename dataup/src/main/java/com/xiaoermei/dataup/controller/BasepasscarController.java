package com.xiaoermei.dataup.controller;


import com.aliyuncs.exceptions.ClientException;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.xiaoermei.dataup.pojo.dto.BasepasscarUpDTO;

import com.xiaoermei.dataup.pojo.dto.MultipartFileUploadDTO;
import com.xiaoermei.dataup.service.IBasepasscarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import xiaoermei.com.common.jsonresult.JsonResult;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/dataup")
@Api(tags = "数据对接")
public class BasepasscarController {


    @Autowired
    private IBasepasscarService basepasscarService;

    /**
     * 此方法是上传数据，不包括图片
     * @param basepasscarUpDTO 上传的过车数据，包括超限的和不超限的
     * @return
     */
    @ApiOperation(value = "过车结构化数据的上传")
    @ApiOperationSupport(order=100)
    @PostMapping("uploadPassVehicle")
    private JsonResult uploadPassVehicle(@RequestBody BasepasscarUpDTO basepasscarUpDTO ){
        basepasscarService.uploadPassVehicle(basepasscarUpDTO);

        return JsonResult.ok();
    }




    @ApiOperation(value = "过车非结构化数据的上传")
    @ApiOperationSupport(order=101)
    @PostMapping("uploadImg")
    private JsonResult uploadImgMultipartFiles( @RequestBody MultipartFileUploadDTO multipartFileUploadDTO) throws ClientException {
        basepasscarService.uploadImgMultipartFiles(multipartFileUploadDTO);
        return JsonResult.ok();
    }





}
