package com.xiaoermei.dataup.dao.cache;

import com.xiaoermei.dataup.pojo.po.SitenamePO;

import java.util.List;

public interface ISiteCacheRepository {
    void rebuildSitenameSearch( List<SitenamePO> sitenamePOS);

    String getSitenameBySitecode(String siteCode);

    void deleteAll();
}
