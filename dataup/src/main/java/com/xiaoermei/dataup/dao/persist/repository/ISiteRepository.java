package com.xiaoermei.dataup.dao.persist.repository;

import com.xiaoermei.dataup.pojo.po.SitenamePO;

import java.util.List;

public interface ISiteRepository {
    List<SitenamePO> ListAllSiteCache();
}
