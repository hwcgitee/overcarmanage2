package com.xiaoermei.dataup.dao.cache.impl;

import com.alibaba.fastjson.JSON;
import com.xiaoermei.dataup.dao.cache.ISiteCacheRepository;
import com.xiaoermei.dataup.pojo.po.SitenamePO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Set;


@Slf4j
@Repository
public class SiteCacheRepositoryImpl implements ISiteCacheRepository {

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public void rebuildSitenameSearch(List<SitenamePO> sitenamePOS) {
        log.debug("开始处理【站点编码和站点名称】的缓存数据访问，地区数据：{}", sitenamePOS);
        for (SitenamePO sitenamePO:sitenamePOS
             ) {
            String key = "兰溪非现场点位:"+ sitenamePO.getSiteCode();
            SetOperations<String, Serializable> opsForSet = redisTemplate.opsForSet();
            //这是为了重启存数据之前先清除历史数据的
            opsForSet.add("KEY_ALL_KEYS", key);
            ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
            opsForValue.set(key, sitenamePO.getSiteName());
        }


    }

    @Override
    public String getSitenameBySitecode(String siteCode) {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        Serializable sitename = opsForValue.get("兰溪非现场点位:"+siteCode);
        String siteName=JSON.toJSONString(sitename);
        return siteName;
    }

    @Override
    public void deleteAll() {
        SetOperations<String, Serializable> opsForSet = redisTemplate.opsForSet();
        Set keys = opsForSet.members("KEY_ALL_KEYS");
        redisTemplate.delete(keys);
    }
}
