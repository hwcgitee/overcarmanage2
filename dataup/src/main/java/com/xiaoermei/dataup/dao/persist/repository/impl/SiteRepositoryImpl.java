package com.xiaoermei.dataup.dao.persist.repository.impl;

import com.xiaoermei.dataup.dao.persist.mapper.SiteMapper;
import com.xiaoermei.dataup.dao.persist.repository.ISiteRepository;
import com.xiaoermei.dataup.pojo.po.SitenamePO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class SiteRepositoryImpl implements ISiteRepository {

    @Autowired
    private SiteMapper siteMapper;

    @Override
    public List<SitenamePO> ListAllSiteCache() {
        List<SitenamePO> sitenamePOS=siteMapper.ListAllSiteCache();
        return sitenamePOS;
    }
}
