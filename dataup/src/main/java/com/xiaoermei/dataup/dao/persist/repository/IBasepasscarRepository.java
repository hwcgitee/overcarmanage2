package com.xiaoermei.dataup.dao.persist.repository;

import com.xiaoermei.dataup.pojo.entity.BasepasscarEntity;

public interface IBasepasscarRepository {
    int uploadPassVehicle(BasepasscarEntity basepasscarEntity);

    int updatemultipartFiles(BasepasscarEntity basepasscarEntity);
}
