package com.xiaoermei.dataup.dao.persist.repository.impl;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoermei.dataup.dao.persist.mapper.BasepasscarMapper;
import com.xiaoermei.dataup.dao.persist.repository.IBasepasscarRepository;
import com.xiaoermei.dataup.pojo.entity.BasepasscarEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class BasepasscarRepositoryImpl implements IBasepasscarRepository {
    @Autowired
    private BasepasscarMapper basepasscarMapper;

    @Override
    public int uploadPassVehicle(BasepasscarEntity basepasscarEntity) {
        return basepasscarMapper.insert(basepasscarEntity);
    }

    @Override
    public int updatemultipartFiles(BasepasscarEntity basepasscarEntity) {

        int num=basepasscarMapper.updateByRecordCode(basepasscarEntity);
        //redis的改造完成了可以用
       // basepasscarMapper.updateById();
        return num;
    }
}
