package com.xiaoermei.dataup.dao.persist.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoermei.dataup.pojo.entity.BasepasscarEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface BasepasscarMapper extends BaseMapper<BasepasscarEntity> {

    //update  t_bas_passcar_data set phone='车牌照',photo1='https://overcarbucket.oss-cn-hangzhou.aliyuncs.com/photo/chetou1.png',photo2='https://overcarbucket.oss-cn-hangzhou.aliyuncs.com/photo/chece1.png',photo3='https://overcarbucket.oss-cn-hangzhou.aliyuncs.com/photo/chewei1.png',
    //        photo4='',photo5='',video='https://overcarbucket.oss-cn-hangzhou.aliyuncs.com/video/video1.mp4'    where record_code='1_20221010235959'
    int updateByRecordCode(BasepasscarEntity basepasscarEntity);
}
