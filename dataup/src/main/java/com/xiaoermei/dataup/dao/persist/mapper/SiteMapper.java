package com.xiaoermei.dataup.dao.persist.mapper;

import com.xiaoermei.dataup.pojo.po.SitenamePO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SiteMapper {
    //select site_code,site_name from t_site;
    List<SitenamePO> ListAllSiteCache();
}
