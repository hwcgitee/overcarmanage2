package com.xiaoermei.dataup.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xiaoermei.dataup.util.UploadMutltifilesToOSS;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

public class ByteArrayUploadTest {

    public static void main(String[] args) {

        String imagePath = "D:\\LenovoSoftstore\\测试的过车数据\\测试的过车数据\\photo\\2_20231010235959_chece2.png";

        // 将图片转换为JSON
        String json = convertImageToJson(imagePath);


        // 解析JSON字符串为JSON对象
        Gson gson = new Gson();
        ImageJsonData jsonData = gson.fromJson(json, ImageJsonData.class);

        // 对Base64编码的图片数据进行解码
        byte[] imageData = Base64.getDecoder().decode(jsonData.getImageData());

        UploadMutltifilesToOSS.upload(imageData);





    }

    // 将图片转换为JSON
    private static String convertImageToJson(String imagePath) {
        try {
            // 读取图片文件
            BufferedImage image = ImageIO.read(new File(imagePath));

            // 创建字节输出流，用于写入Base64编码后的图片数据
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);

            // 对图片数据进行Base64编码
            byte[] imageData = baos.toByteArray();
            String base64Image = Base64.getEncoder().encodeToString(imageData);

            // 构建包含图片数据的JSON对象
            ImageJsonData jsonData = new ImageJsonData();
            jsonData.setImageData(base64Image);

            // 将JSON对象转换为JSON字符串
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 定义用于存储图片数据的JSON对象
    private static class ImageJsonData {
        private String imageData;

        public String getImageData() {
            return imageData;
        }

        public void setImageData(String imageData) {
            this.imageData = imageData;
        }
    }





}
