package com.xiaoermei.dataup.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MultipartFileUploadDTO implements Serializable {
    //"检测单号", 该检测单号需与过车检测数据检测单号保持一致。
    @ApiModelProperty(value="检测单号，就是流水号",required = true,example = "2_20231010235959")
    private String checkNo;//recordCode
    @ApiModelProperty(value="传输的字符串",required = false)
    //"照片传输方式", //字符串，参数：2 ；定义为上传图片、视频文件经 过 BASE64 转码后的字符串；
    private String transType;//无
    //“站点类型”，//21：超限检测站；31:非现场检测点;41:高速公路入口 检测;42:高速公路出口检测;71：重点货运源头企业
    @ApiModelProperty(value="站点类型",required = true,example = "31")
    private Integer siteType;//siteType
    //"检测照片 1 车牌小图",
    @ApiModelProperty(value="车牌小图",required = false)
    private String vehiclepic1;//photo
    @ApiModelProperty(value="车头照片",required = false,example = "2_20231010235959_chetou1.png")
    // "检测照片 2 车头照片",
    private String vehiclepic2;//photo1
    //"检测照片 3 侧拍照片",
    @ApiModelProperty(value="车侧照片",required = false,example = "2_20231010235959_chece1.png")
    private String vehiclepic3;//photo2
    //"检测照片 4 车尾照片",
    @ApiModelProperty(value="车尾照片",required = false,example = "2_20231010235959_chewei1.png")
    private String vehiclepic4;//photo3
    //"检测照片 5 前侧照片",
    @ApiModelProperty(value="前侧照片",required = false)
    private String vehiclepic5;//photo4
    //"检测照片 6 后侧照片
    @ApiModelProperty(value="后侧照片",required = false)
    private String vehiclepic6;//photo5
    //短视频"
    @ApiModelProperty(value="短视频",required = false,example = "2_20231010235959_video1.mp4")
    private String shortvedio;//video
    //视频后缀类型",// 1:mp4 格式 ；2：avi 格式 其他的再定
    @ApiModelProperty(value="视频后缀",required = false)
    private String suffixType;//无

}
