package com.xiaoermei.dataup.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Value;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class BasepasscarUpDTO implements Serializable {
    //"检测单号", //数据唯一标识，为避免重复，请在检测单号前添加站点的
    //编号和该条记录的检测时间“站点编号_检测时间”，例如：3301093101_20221010235959130--这里举例都举错了，至少上下矛盾
    @ApiModelProperty(value="检测单号，就是流水号",required = true,example = "1_20221010235959")
    private String checkNo;//它对应的字段是实体类中的recordCode这个属性
    @ApiModelProperty(value="站点编号",required = true,example = "1")
    //"站点编号", //关联站点标识。需对接站点统一上报，由省级系统统一授 权安排站点编号。
    private String siteCode;//在别的表里面
    //“站点类型”，//21：超限检测站 31:非现场检测点;41:高速公路入口检 测;42:高速公路出口检测; 71:重点源头企业
    @ApiModelProperty(value="站点类型",required = true,example = "31")
    private Integer siteType;//siteType
    //“检测数据类型”，//站点类型为 21 时 检测类型必填 检测类型: 1：初 检； 2：复检
    @ApiModelProperty(value="检测数据类型",required = false,example = "1")
    private Integer checkType;//没有
    //"检测车道编号", //关联检测车道标识。按行进方向从左到右排列，依 次为 01、02
    @ApiModelProperty(value="检测车道编号",required = true,example = "01")
    private String laneNumber;//laneCode
    //"检测时间", //字符串，格式为：yyyyMMddHHmmss 例如：20221010235959
    @ApiModelProperty(value="检测时间",required = true,example = "20221010235959")
    private Date checkTime;//似乎是这个insertTime????
    //"车牌号牌", //识别出的号牌号码。格式为：浙 A12345
    @ApiModelProperty(value="车牌号牌",required = false,example = "浙A12345")
    private String vehicleNo;//carNo
    //"车牌颜色", //0-蓝色；1-黄色；2-黑色；3-白色；4-渐变绿色；5黄绿双拼色；6-蓝白渐变色；9-未确定；11-绿色；12-红色
    @ApiModelProperty(value="车牌颜色",required = false,example = "1")
    private Integer vehicleColor;//carNoColor
    //"号牌种类", //01-大型汽车号牌；02-小型汽车号牌；
    @ApiModelProperty(value="号牌种类",required = false,example = "01")
    private String vehicleNoType;//无
    //"行驶方向", //0-上行；1-下行
    @ApiModelProperty(value="行驶方向",required = true,example = "0")
    private Integer travelDirection;//无
    //"车速", // Integer,精确到个位。单位：km/h
    @ApiModelProperty(value="车速",required = true,example = "22.00")
    private BigDecimal speed;//speed
    //"车轴数量", // Integer,轴数不能小于 2
    @ApiModelProperty(value="车轴数量",required = true,example = "4")
    private Integer axles;//axis
    //"轴组类型" 对应公路货运车辆车型代码,
    @ApiModelProperty(value="轴组类型",required =false)
    private String axlesType;//axisType
    //"车货总质量", // Integer,精确到个位。单位：千克
    @ApiModelProperty(value="车货总质量",required = true,example = "49000")
    private Integer total;//totalWeight
    //"最大允许总质量", // Integer,精确到个位。单位：千克
    @ApiModelProperty(value="最大允许总质量",required = false,example = "36000")
    private Integer limitWeight;//limitWeight
    //"超限量", //Integer,精确到个位。单位：千克
    @ApiModelProperty(value="超限量",required = false,example = "130000")
    private Integer overWeight;//overrun
    //"超限超载率", //Double,精确到小数点后 2 位。单位：%
    @ApiModelProperty(value="超限超载率",required = false,example = "33.33")
    private BigDecimal overRate;//overrunRate
    //"车辆品牌", //例如：解放牌 ，非必填
    @ApiModelProperty(value="车辆品牌",required = false)
    private String vehicleBrand;//无
    //"车辆类型" //填写车辆类型代码，参见《车辆类型代码》 ，非必填
    @ApiModelProperty(value="车辆类型",required = false)
    private Integer vehicleType;//carType
}
