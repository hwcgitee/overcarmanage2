package com.xiaoermei.dataup.pojo.po;

import lombok.Data;

import java.io.Serializable;


@Data
public class SitenamePO implements Serializable {
    private String siteName;
    private String siteCode;
    //在数据上传的时候可以查这个字段也可以不查
    //private String deptName;
}
