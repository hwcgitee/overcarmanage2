package com.xiaoermei.dataup.pojo.po;

import lombok.Data;

import java.io.Serializable;


@Data
public class UndealCarPO implements Serializable {
    //车牌号
    private String carNo;
    //车牌颜色
    private String carNoColor;
    //未处理记录

}
