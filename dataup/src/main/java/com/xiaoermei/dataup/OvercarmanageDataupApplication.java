package com.xiaoermei.dataup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("applicationContext.xml")
public class OvercarmanageDataupApplication {
    public static void main(String[] args) {
        SpringApplication.run(OvercarmanageDataupApplication.class,args);
    }
}
