// 组件名是my-header,复用每个页面的头部
Vue.component('my-header',{
    data:function () {
        //双向绑定写这里
        return{
           user:localStorage.user?JSON.parse(localStorage.user):null,
            currentIndex:"",
            wd:"",
        }
    },


    methods:{
        search(){
            // 复用list.html
            location.href="/list.html?wd="+this.wd;
        },

        handleSelect(key,keypath){
          if ((key==0)){
              location.href="/";
          }else {
              location.href="/list.html?type="+key
          }
        },
       logout(){
           if(confirm("您确定退出登录吗")){
               axios.get("/v1/users/logout").then(function (response) {
                   if (response.data.code==1){
                       localStorage.clear();
                       location.href="/login.html";
                   }
               })
           }

       }
    },

    created:function () {
        if(location.pathname=="/"||location.pathname=="index.html"){
            this.currentIndex="0";
        }else {
            if(location.search.includes("type")){
                let type=new URLSearchParams(location.search).get("type");
                this.currentIndex=type;
            }
        }

    },



    // 自定义组件的内容，esc上的反引号
    template:` <el-header height="80px">
      <div style="width: 1200px;margin: 0 auto">
        <el-row gutter="20">
        <el-col span="6">
          <img src="imgs/icon.png" width="200">
        </el-col>
        <el-col span="10">
          <el-menu @select="handleSelect" :default-active="currentIndex"
          mode="horizontal" active-text-color="orange">
            <el-menu-item index="0">首页</el-menu-item>
            <el-menu-item index="1">食谱</el-menu-item>
            <el-menu-item index="2">视频</el-menu-item>
            <el-menu-item index="3">资讯</el-menu-item>
          </el-menu>
        </el-col>
        <el-col span="6">
<!--        wd是word-->
          <el-input v-model="wd" style="position: relative;top: 15px" placeholder="请输入搜索的关键字">
            <el-button slot="append" icon="el-icon-search" @click="search()"></el-button>
          </el-input>
        </el-col>
        <el-col span="2">
          <el-popover v-if="user==null"
                  placement="top-start"
                  title="欢迎来到烘焙坊!"
                  width="200"
                  trigger="hover">
            <!--设置初始显示的内容-->
            <div slot="reference">
              <i style="font-size: 30px;position: relative;top: 20px" class="el-icon-user"></i>
            </div>
            <!--设置弹出的内容-->
            <el-button type="info" @click="location.href='/reg.html'">注册</el-button>
            <el-button type="warning" @click="location.href='/login.html'">登录</el-button>
          </el-popover>
            <el-popover v-else
                  placement="top-start"
                  title="欢迎来到烘焙坊!"
                  width="200"
                  trigger="hover">
            <!--设置初始显示的内容-->
            <div slot="reference">
              <i style="font-size: 30px;position: relative;top: 20px" class="el-icon-user"></i>
            </div>
            <!--设置弹出的内容-->
            <div style="text-align: center">
            <el-avatar :src="user.imgUrl"> </el-avatar> <br>
                 <a href="/personal.html">个人中心</a>
           <a href="javascript:void(0)" @click="logout()">退出登录</a> <br>
           <a v-if="user.isAdmin==1" href="/admin.html">后台管理页面</a>
            </div>
        
          </el-popover>
        </el-col>
      </el-row>
      </div>
    </el-header>`

})